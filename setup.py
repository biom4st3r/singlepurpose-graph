from setuptools import Extension
from setuptools import setup

setup(
    name='sayhello',
    version='0.0.1',
    python_requires='>=3.6.5',
    build_zig=True,
    ext_modules=[Extension('sayhello', ['./src/Pythonops.zig'])],
    setup_requires=['setuptools-zig']
)
