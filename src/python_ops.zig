const py = @cImport({
   @cDefine("PY_SSIZE_T_CLEAN", {});
//    @cInclude("Python.h");
});

const py = @import("Python.zig");
const libgraph = @import("graph.zig");
// https://docs.python.org/3/extending/extending.html
fn hello(self: ?*py.PyObject, args: ?*py.PyObject) callconv(.C) *py.PyObject {
    _ = self;
    _ = args;
    return py.Py_BuildValue("i", @as(c_int, 33));
}

var someMethods = [_]py.PyMethodDef {
    py.PyMethodDef{
        .ml_name = "sayhello",
        .ml_meth = hello,
        .ml_flags = py.METH_NOARGS,
        .ml_doc = "Say hello to zig",
    },
};

var amodule = py.PyModuleDef{
    .m_base = py.PyModuleDef_Base{
        .ob_base = py.PyObject{
            .ob_refcnt = 1,
            .ob_type = null,
        },
        .m_init = null,
        .m_index = 0,
        .m_copy = null,
    },
    .m_name = "zighello",
    .m_doc = null,
    .m_size = -1,
    .m_methods = &someMethods,
    .m_slots = null,
    .m_traverse = null,
    .m_clear = null,
    .m_free = null,
};
// https://docs.python.org/3/extending/newtypes_tutorial.html

// static PyTypeObject CustomType = {
//     PyVarObject_HEAD_INIT(NULL, 0)
//     .tp_name = "custom2.Custom",
//     .tp_doc = PyDoc_STR("Custom objects"),
//     .tp_basicsize = sizeof(CustomObject),
//     .tp_itemsize = 0,
//     .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
//     .tp_new = Custom_new,
//     .tp_init = (initproc) Custom_init,
//     .tp_dealloc = (destructor) Custom_dealloc,
//     .tp_members = Custom_members,
//     .tp_methods = Custom_methods,
// };

fn graphHolderInit(type: *py.PyTypeObject, args: *py.PyObject, kwds: *py.PyObject) *py.PyObject {
    var width: usize = 0;
    var height: usize = 0;
    var map: [][]const u8 = undefined;
    py.PyArg_ParseTuple(args, "width", &width);
    py.PyArg_ParseTuple(args, "height", &height);
    return null;
}

var GraphHolder = py.PyTypeObject{
    .tp_name = "zig.catan.Graph",
    .tp_basicsize = @sizeOf(libgraph.Graph),
    .tp_itemsize = 0,
    .tp_doc = null,
    .tp_new = null,
    .tp_init = graphHolderInit,
    .tp_dealloc = null,
    .tp_members = null,
    .tp_methods = null,
};

pub export fn PyInit_sayhello() [*]py.PyObject {
    var module: *py.PyModuleDef = &amodule;
    py.PyModule_AddObject(module, "Graph", &GraphHolder);

    return module;
}