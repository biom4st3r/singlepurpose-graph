
const std = @import("std");
const print = std.debug.print;
const libgraph = @import("graph.zig");
const Graph = libgraph.Graph;
const assert = std.debug.assert;

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var mask: [5][]const u8 = .{
        "01230",
        "4561",
        "23456",
        "1234",
        "05610"
    };
    var graph = try Graph.create(5, 5, &mask, gpa.allocator());
    defer graph.deinit();

    if (graph) {
        print("\n{} Tiles | {} Roads | {} Houses\n", .{graph.tiles.len, graph.roads.i, graph.houses.i});
    } else |err| {
        print("\nErr {}\n", .{err});
    }
    assert( graph.roads.i == 72 );
    assert( graph.houses.i == 54 );
    var buf: [10]u8 = undefined;
    _ = try std.io.getStdIn().reader().readUntilDelimiterOrEof(buf[0..], '\n');
    // print("done. graphsize: {}\n | {}", .{@sizeOf(Graph)});
}

test "graph" {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    var mask: [5][]const u8 = .{
        "01230",
        "4561",
        "23456",
        "1234",
        "05610"
    };
    var graph = try Graph.create(5, 5, &mask, gpa.allocator());
    defer graph.deinit();

    if (graph) {
        print("\n{} Tiles | {} Roads | {} Houses\n", .{graph.tiles.len, graph.roads.i, graph.houses.i});
    } else |err| {
        print("\nErr {}\n", .{err});
    }
    assert( graph.roads.i == 72 );
    assert( graph.houses.i == 54 );
    var buf: [10]u8 = undefined;
    _ = try std.io.getStdIn().reader().readUntilDelimiterOrEof(buf[0..], "\n");
}
