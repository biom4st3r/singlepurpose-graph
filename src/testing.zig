

// fn printstuff(arr: [][]const u8) void { // slice argument
//     for (arr) |ele| { // derefence pointer to iterate
//         std.debug.print("{s}\n", .{ele}); // print each element
//     }
// }

// test "are you a string" {
//     var f0 = [_][]const u8{"one", "two"};
//     printstuff(&f0);         // error: ...but found *[2][]const u8

//     var f1 = [_][]const u8{"one", "two"};
//     printstuff(f1[0..]);     // error: ...but found *[2][]const u8

//     var f2: [2][]const u8 = .{"one", "two"};
//     printstuff(&f2);         // error: ...but found *struct:131:15

//     var f3: [2][]const u8 = .{"one", "two"};
//     printstuff(f3[0..]);     // error: slice of non-array type 'struct:134:15'

//     // var f4 = [_][]const u8{ "one", "two" };
//     // var f4_1: [][]const u8 = f4[0..];
//     // printstuff(&f4_1);       // success, but dumb

//     // var f5 = [_][]const u8{ "one", "two" };
//     // var f5_1: [][]const u8 = &f5; // error: ...but found '*[2][]const u8'
//     // printstuff(f5_1);
// }





// test "type" {
//     var string = .{
//         "One", 
//         "Two"
//     };
//     std.debug.print("{}", .{@TypeOf(string)});
// }

// test "WTF ARE YOU" {
//     var uncoerced = "fffff";
//     var coerced: []const u8 = uncoerced;
//     std.debug.print("\nuncoerced string type: {}\n", .{@TypeOf(uncoerced)});
//     std.debug.print("coerced string type: {}\n", .{@TypeOf(coerced)});

//     // Compile Error?
//     // var tdarray_of_uncoerced: [3][]const u8 = [_][]const u8 {
//     //     "aaaaa",
//     //     "bbbbb",
//     //     "ccccc",
//     // }; 

//     var tdarray_of_sliced: [][]const u8 = ([_][]const u8 {
//         "aaaaa"[0..],
//         "bbbbb"[0..],
//         "ccccc"[0..],
//     })[0..];

//     std.debug.print("2d array of sliced strings: {}\n", .{@TypeOf(tdarray_of_sliced)});

//     var tdarray_of_coerced: [3][]const u8 = [_][]const u8 {
//         &("aaaaa".*),
//         &("bbbbb".*),
//         &("ccccc".*),
//     };
//     std.debug.print("2d array of derefed -> refed strings: {}\n", .{@TypeOf(tdarray_of_coerced)});
//     std.debug.print("slice of 2d array: {}\n", .{@TypeOf(tdarray_of_coerced[0..])});

//     var tdarrayslice: [][]const u8 = tdarray_of_coerced[0..];
//     std.debug.print("coerced slice of 2d array: {}\n", .{@TypeOf(tdarrayslice)});

//     // Synatax Error slicing array?
//     // var tdarray: [][]const u8 = [_][]const u8 {
//     //     "aaaaa",
//     //     "bbbbb",
//     //     "ccccc",
//     // }[0..];
//     // std.debug.print("{}\n", .{@TypeOf(tdarray)});
// }



// test "test?" {
//     // Reproduces the crash
//     var gpa = std.heap.GeneralPurposeAllocator(.{}){};
//     var alloc = gpa.allocator();
//     var roads = std.ArrayList(Road).init(alloc);
//     defer roads.deinit();
//     var tiles: [][]?Tile = try mdarray(?Tile,5,5,&alloc);
//     defer alloc.free(tiles);
//     for (range(5)) |_,y| {
//         for(range(5)) |_,x| {
//             tiles[y][x] = Tile{
//                 .tile_type = 255,
//                 .x = @intCast(u8, x),
//                 .y = @intCast(u8, y),
//             };
//             var tile = &tiles[y][x].?;
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 0),
//             }) catch unreachable;
//             tile.*.setTopLeftEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 1),
//             }) catch unreachable;
//             tile.*.setTopRightEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 2),
//             }) catch unreachable;
//             tile.*.setRightEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 3),
//             }) catch unreachable;
//             tile.*.setBottomRightEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 4),
//             }) catch unreachable;
//             tile.*.setBottomLeftEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 5),
//             }) catch unreachable;
//             tile.*.setLeftEdge(&roads.items[roads.items.len - 1]);
//         }
//     }

//     for (range(5)) |_,y| {
//         for(range(5)) |_,x| {
//             var tile = &tiles[y][x].?;
//             tile.*.getTopLeftEdge().?.addNeighbor(tile.*.getLeftEdge().?);
//             tile.*.getTopLeftEdge().?.addNeighbor(tile.*.getTopRightEdge().?);

//             tile.*.getTopRightEdge().?.addNeighbor(tile.*.getTopLeftEdge().?);
//             tile.*.getTopRightEdge().?.addNeighbor(tile.*.getRightEdge().?);

//             tile.*.getRightEdge().?.addNeighbor(tile.*.getTopRightEdge().?);
//             tile.*.getRightEdge().?.addNeighbor(tile.*.getBottomRightEdge().?);

//             tile.*.getBottomRightEdge().?.addNeighbor(tile.*.getRightEdge().?);
//             tile.*.getBottomRightEdge().?.addNeighbor(tile.*.getBottomLeftEdge().?);

//             tile.*.getBottomLeftEdge().?.addNeighbor(tile.*.getBottomRightEdge().?);
//             tile.*.getBottomLeftEdge().?.addNeighbor(tile.*.getLeftEdge().?);

//             tile.*.getLeftEdge().?.addNeighbor(tile.*.getBottomLeftEdge().?);
//             tile.*.getLeftEdge().?.addNeighbor(tile.*.getTopLeftEdge().?);
//         }
//     }
// }



// test "segfault" {
    // @breakpoint();
    // var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    // var alloc: Allocator = gpa.allocator();
    // var list = std.ArrayList(Chain).init(alloc);
    // // var heap = DumbHeap(Chain, &alloc){};
    
    // list.append(Chain{.id=0}) catch unreachable;
    // var original: *Chain = &list.items[list.items.len - 1];
    // var prev: *Chain = original;
    
    // // var original: *Chain = heap.append(Chain{.id=0});
    // // var prev: *Chain = original;
    // for (range(8)) |_,i| {
    //     list.append(Chain{.id=i+1}) catch unreachable;
    //     // prev.ref = heap.append(Chain{.id=i+1});
    //     prev = prev.ref;
    // }
    // print("{}", .{original.*.id});
// }

// test "heap_test_segfault" {
//     var gpa = std.heap.GeneralPurposeAllocator(.{}){};
//     var heap = try DumbHeap(Chain).init(gpa.allocator());
//     const original: *Chain = try heap.append(Chain{.id=0});
//     var prev: *Chain = heap.get(0);
//     for (range(255)) |_,i| {
//         prev.ref = try heap.append(Chain{.id=i+1});
//         prev = prev.ref;
//     }
//     prev = original;
//     for (range(254)) |_| {
//         print("\n {}", .{prev.id});
//         prev = prev.ref;
//         assert(prev.id + 1 == prev.ref.id);
//     }
// }



// test "test?" {
//     // Reproduces the crash
//     var gpa = std.heap.GeneralPurposeAllocator(.{}){};
//     var alloc = gpa.allocator();
//     var roads = std.ArrayList(Road).init(alloc);
//     defer roads.deinit();
//     var tiles: [][]?Tile = try mdarray(?Tile,5,5,&alloc);
//     defer alloc.free(tiles);
//     for (range(5)) |_,y| {
//         for(range(5)) |_,x| {
//             tiles[y][x] = Tile{
//                 .tile_type = 255,
//                 .x = @intCast(u8, x),
//                 .y = @intCast(u8, y),
//             };
//             var tile = &tiles[y][x].?;
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 0),
//             }) catch unreachable;
//             tile.*.setTopLeftEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 1),
//             }) catch unreachable;
//             tile.*.setTopRightEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 2),
//             }) catch unreachable;
//             tile.*.setRightEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 3),
//             }) catch unreachable;
//             tile.*.setBottomRightEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 4),
//             }) catch unreachable;
//             tile.*.setBottomLeftEdge(&roads.items[roads.items.len - 1]);
//             roads.append(Road{
//                 .id=@intCast(u8, (y*5) + x + 5),
//             }) catch unreachable;
//             tile.*.setLeftEdge(&roads.items[roads.items.len - 1]);
//         }
//     }

//     for (range(5)) |_,y| {
//         for(range(5)) |_,x| {
//             var tile = &tiles[y][x].?;
//             tile.*.getTopLeftEdge().?.addNeighbor(tile.*.getLeftEdge().?);
//             tile.*.getTopLeftEdge().?.addNeighbor(tile.*.getTopRightEdge().?);

//             tile.*.getTopRightEdge().?.addNeighbor(tile.*.getTopLeftEdge().?);
//             tile.*.getTopRightEdge().?.addNeighbor(tile.*.getRightEdge().?);

//             tile.*.getRightEdge().?.addNeighbor(tile.*.getTopRightEdge().?);
//             tile.*.getRightEdge().?.addNeighbor(tile.*.getBottomRightEdge().?);

//             tile.*.getBottomRightEdge().?.addNeighbor(tile.*.getRightEdge().?);
//             tile.*.getBottomRightEdge().?.addNeighbor(tile.*.getBottomLeftEdge().?);

//             tile.*.getBottomLeftEdge().?.addNeighbor(tile.*.getBottomRightEdge().?);
//             tile.*.getBottomLeftEdge().?.addNeighbor(tile.*.getLeftEdge().?);

//             tile.*.getLeftEdge().?.addNeighbor(tile.*.getBottomLeftEdge().?);
//             tile.*.getLeftEdge().?.addNeighbor(tile.*.getTopLeftEdge().?);
//         }
//     }
// }
