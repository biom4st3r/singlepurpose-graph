const std = @import("std");
const Allocator = std.mem.Allocator;
const print = std.debug.print;
const assert = std.debug.assert;

const str_ref: type = *const []u8;
const str: type = []const u8;

const util = @import("utils.zig");
const range = util.range;
const mdarray = util.mdarray;
const badintparse = util.badintparse;

pub const Graph = struct {
    alloc: Allocator,
    roads: util.SegArray(Road),
    houses: util.SegArray(House),
    tiles: [][]?Tile = undefined,
    _house_id: u16 = 0,
    _road_id: u16 = 0,

    pub fn deinit(self: *Graph) void {
        self.roads.deinit();
        self.houses.deinit();
        self.alloc.free(self.tiles);
    }

    pub fn create(width: usize, height: usize, mask: [][]const u8, mem: Allocator) !Graph {
        var l = std.ArrayList(Road).init(mem);
        if (l.items.len == -1) {}
        var graph: Graph = Graph{
            .alloc = mem,
            .roads = try util.SegArray(Road).init(mem),
            .houses = try util.SegArray(House).init(mem),
        };

        try graph.createTiles(mask, width, height, mem);
        graph.initEdges(width, height);
        graph.linkEdges();
        graph.initHouses(width,height);
        return graph;
    }

    fn initHouses(self: *Graph, width: usize, height: usize) void {
        for (range(height)) |_,y| {
            for (range(width)) |_,x| {
                // print("\n{}, {}", .{x,y});
                if (y & 1 == 0) {
                    self.initStandardRowHouse(@intCast(i16, x),@intCast(i16, y));
                    self.linkStandardRowHouse(@intCast(i16, x),@intCast(i16, y));
                } else {
                    self.initOtherRowHouse(@intCast(i16, x),@intCast(i16, y));
                    self.linkOtherRowHouse(@intCast(i16, x),@intCast(i16, y));
                }
            }
        }
    }

    fn linkStandardRowHouse(self: *Graph, x: i16, y: i16) void {
        var current: *Tile = if (self.getTile(x, y)) |t| t else return;
        var house: *House = undefined;

        house = current.getTopHouse().?;
        house.linkToRoad(current.getTopLeftEdge().?);
        house.linkToRoad(current.getTopRightEdge().?);
        if (self.getTile(x, y - 1)) |tile| house.linkToRoad(tile.getLeftEdge().?) 
            else if (self.getTile(x - 1, y - 1)) |tile| house.linkToRoad(tile.getRightEdge().?) else {}
        
        house = current.getTopRightHouse().?;
        house.linkToRoad(current.getTopRightEdge().?);
        house.linkToRoad(current.getRightEdge().?);
        if (self.getTile(x, y - 1)) |tile| house.linkToRoad(tile.getBottomRightEdge().?) 
            else if (self.getTile(x + 1, y)) |tile| house.linkToRoad(tile.getTopLeftEdge().?) else {}

        house = current.getBottomRightHouse().?;
        house.linkToRoad(current.getRightEdge().?);
        house.linkToRoad(current.getBottomRightEdge().?);
        if (self.getTile(x + 1, y)) |tile| house.linkToRoad(tile.getBottomLeftEdge().?) 
            else if (self.getTile(x, y + 1)) |tile| house.linkToRoad(tile.getTopRightEdge().?) else {}
        
        house = current.getBottomHouse().?;
        house.linkToRoad(current.getBottomLeftEdge().?);
        house.linkToRoad(current.getBottomRightEdge().?);
        if (self.getTile(x - 1, y + 1)) |tile| house.linkToRoad(tile.getRightEdge().?) 
            else if (self.getTile(x, y + 1)) |tile| house.linkToRoad(tile.getLeftEdge().?) else {}

        house = current.getBottomLeftHouse().?;
        house.linkToRoad(current.getLeftEdge().?);
        house.linkToRoad(current.getBottomLeftEdge().?);
        if (self.getTile(x - 1, y + 1)) |tile| house.linkToRoad(tile.getTopLeftEdge().?) 
            else if (self.getTile(x - 1, y)) |tile| house.linkToRoad(tile.getBottomRightEdge().?) else {}

        house = current.getTopLeftHouse().?;
        house.linkToRoad(current.getLeftEdge().?);
        house.linkToRoad(current.getTopLeftEdge().?);
        if (self.getTile(x - 1, y - 1)) |tile| house.linkToRoad(tile.getBottomLeftEdge().?) 
            else if (self.getTile(x - 1, y)) |tile| house.linkToRoad(tile.getTopRightEdge().?) else {}
    }

    fn createHouse(self: *Graph) *House {
        var ele = self.houses.append(House{.id = self._house_id}) catch unreachable;
        self._house_id += 1;
        return ele;
    }

    fn getOrCreateHouse(self: *Graph, possibleHouses: []?*House) *House {
        for (possibleHouses) |ele| {
            if (ele == null) continue;
            return ele.?;
        }
        return self.createHouse();
    }

    fn initStandardRowHouse(self: *Graph, x: i16, y: i16) void {
        var current: *Tile = if (self.getTile(x, y)) |tile| tile else return;
        var options: [2]?*House = .{undefined} ** 2;

        options = .{
            if (self.getTile(x, y - 1)) |tile| tile.getBottomLeftHouse() else null, 
            if (self.getTile(x - 1, y - 1)) |tile| tile.getBottomRightHouse() else null,
        };
        current.setTopHouse(self.getOrCreateHouse(&options));

        options = .{
            if (self.getTile(x, y - 1)) |tile| tile.getBottomHouse() else null,
            null
        };
        current.setTopRightHouse(self.getOrCreateHouse(&options));

        current.setBottomRightHouse(self.createHouse());

        current.setBottomHouse(self.createHouse());
    
        options = .{
            if (self.getTile(x - 1, y)) | tile | tile.getBottomRightHouse() else null,
            null,
        };
        current.setBottomLeftHouse(self.getOrCreateHouse(&options));
    
        options = .{
            if (self.getTile(x - 1, y)) | tile | tile.getTopRightHouse() else null,
            if (self.getTile(x - 1, y - 1)) | tile | tile.getBottomHouse() else null,
        };
        current.setTopLeftHouse(self.getOrCreateHouse(&options));
    }

    fn linkOtherRowHouse(self: *Graph, x: i16, y: i16) void {
        var current: *Tile = if (self.getTile(x, y)) | tile | tile else return;
        var house: *House = undefined;
        house = current.getTopHouse().?;
        house.linkToRoad(current.getTopLeftEdge().?);
        house.linkToRoad(current.getTopRightEdge().?);
        if (self.getTile(x, y - 1)) |tile| house.linkToRoad(tile.getRightEdge().?) 
            else if (self.getTile(x + 1, y - 1)) |tile| house.linkToRoad(tile.getLeftEdge().?) else {}

        house = current.getTopRightHouse().?;
        house.linkToRoad(current.getTopRightEdge().?);
        house.linkToRoad(current.getRightEdge().?);
        if (self.getTile(x + 1, y - 1)) |tile| house.linkToRoad(tile.getBottomRightEdge().?) 
            else if (self.getTile(x + 1, y)) |tile| house.linkToRoad(tile.getTopLeftEdge().?) else {}

        house = current.getBottomRightHouse().?;
        house.linkToRoad(current.getRightEdge().?);
        house.linkToRoad(current.getBottomRightEdge().?);
        if (self.getTile(x + 1, y)) |tile| house.linkToRoad(tile.getBottomLeftEdge().?) 
            else if (self.getTile(x + 1, y + 1)) |tile| house.linkToRoad(tile.getTopRightEdge().?) else {}

        house = current.getBottomHouse().?;
        house.linkToRoad(current.getBottomLeftEdge().?);
        house.linkToRoad(current.getBottomRightEdge().?);
        if (self.getTile(x + 1, y + 1)) |tile| house.linkToRoad(tile.getLeftEdge().?) 
            else if (self.getTile(x, y + 1)) |tile| house.linkToRoad(tile.getRightEdge().?) else {}

        house = current.getBottomLeftHouse().?;
        house.linkToRoad(current.getLeftEdge().?);
        house.linkToRoad(current.getBottomLeftEdge().?);
        if (self.getTile(x - 1, y)) |tile| house.linkToRoad(tile.getBottomRightEdge().?) 
            else if (self.getTile(x - 1, y - 1)) |tile| house.linkToRoad(tile.getTopLeftEdge().?) else {}

        house = current.getTopLeftHouse().?;
        house.linkToRoad(current.getLeftEdge().?);
        house.linkToRoad(current.getTopLeftEdge().?);
        if (self.getTile(x - 1, y)) |tile| house.linkToRoad(tile.getTopRightEdge().?) 
            else if (self.getTile(x, y - 1)) |tile| house.linkToRoad(tile.getBottomLeftEdge().?) else {}
    }

    fn initOtherRowHouse(self: *Graph, x: i16, y: i16) void {
        var current: *Tile = if (self.getTile(x, y)) | tile | tile else return;
        var options: [2]?*House = .{undefined} ** 2;

        options = .{
            if (self.getTile(x, y - 1)) |tile| tile.getBottomRightHouse() else null, 
            if (self.getTile(x + 1, y - 1)) |tile| tile.getBottomLeftHouse() else null,
        };
        current.setTopHouse(self.getOrCreateHouse(&options));

        options = .{
            if (self.getTile(x + 1, y - 1)) |tile| tile.getBottomHouse() else null,
            null
        };
        current.setTopRightHouse(self.getOrCreateHouse(&options));

        current.setBottomRightHouse(self.createHouse());

        current.setBottomHouse(self.createHouse());

        options = .{
            if (self.getTile(x - 1, y)) | tile | tile.getBottomRightHouse() else null,
            null,
        };
        current.setBottomLeftHouse(self.getOrCreateHouse(&options));
    
        options = .{
            if (self.getTile(x - 1, y)) | tile | tile.getTopRightHouse() else null,
            if (self.getTile(x, y - 1)) | tile | tile.getBottomHouse() else null,
        };
        current.setTopLeftHouse(self.getOrCreateHouse(&options));
    }

    fn linkEdges(self: *Graph) void {
        for (self.tiles) |row,y| {
            for (row) |_,x| {
                var tile: *Tile = if (self.getTile(@intCast(i16, x), @intCast(i16, y))) |t| t else continue;
                // print("\n{}, {}", .{x,y});
                var edge: *Road = undefined;
                edge = tile.getTopLeftEdge().?;
                edge.addNeighbor(tile.getLeftEdge().?);
                edge.addNeighbor(tile.getTopRightEdge().?);

                edge = tile.getTopRightEdge().?;
                edge.addNeighbor(tile.getTopLeftEdge().?);
                edge.addNeighbor(tile.getRightEdge().?);

                edge = tile.getRightEdge().?;
                edge.addNeighbor(tile.getTopRightEdge().?);
                edge.addNeighbor(tile.getBottomRightEdge().?);

                edge = tile.getBottomRightEdge().?;
                edge.addNeighbor(tile.getRightEdge().?);
                edge.addNeighbor(tile.getBottomLeftEdge().?);

                edge = tile.getBottomLeftEdge().?;
                edge.addNeighbor(tile.getBottomRightEdge().?);
                edge.addNeighbor(tile.getLeftEdge().?);

                edge = tile.getLeftEdge().?;
                edge.addNeighbor(tile.getBottomLeftEdge().?);
                edge.addNeighbor(tile.getTopLeftEdge().?);
            }
        }
    }

    fn createTiles(self: *Graph, mask: [][]const u8, width: usize, height: usize, mem: Allocator) !void {
        self.tiles = try mdarray(?Tile, height, width, mem);
        for (mask) |arr, y| {
            for (arr) |t, x| {
                const tt = badintparse(t);
                // print("\n({},{} | {})", .{x,y,tt});
                self.tiles[y][x] = if (tt == 0) null else Tile{
                    .x=@intCast(u8, x),
                    .y=@intCast(u8, y),
                    .tile_type=tt,
                };
            }
        }
    }

    fn initEdges(self: *Graph, width: usize, height: usize) void {
        for (range(height)) |_,y| {
            for(range(width)) |_,x| {
                if (y & 1 == 0) {
                    self.initStandardRowEdge(@intCast(i16, x),@intCast(i16, y));
                } else { // other
                    self.initOtherRowEdge(@intCast(i16, x),@intCast(i16, y));
                }
            }
        }
    }

    fn createEdge(self: *Graph) *Road {
        // print(" Edge[{}] ", .{self._road_id});
        var ele = self.roads.append(Road{
            .id = self._road_id,
        }) catch unreachable;
        self._road_id += 1;
        return ele;
    }

    fn getTile(self: *Graph, x: i16, y: i16) ?*Tile {
        if (x < 0 or y < 0) {
            return null;
        }
        if (y >= self.tiles.len) return null;
        if (x >= self.tiles[@intCast(usize, y)].len) return null;
        var tile: *?Tile = &self.tiles[@intCast(usize, y)][@intCast(usize, x)];
        if (tile.* orelse null == null) return null;
        return &tile.*.?;
    }

    fn setOrCreateEdge(self: *Graph, parent: *Tile, x: i16, y: i16, getEdge: fn(*Tile) ?*Road, setEdge: fn(*Tile, *Road) void) void {
        var nei = self.getTile(x, y);
        var nei_road = if (nei != null) getEdge(nei.?) else null;
        // print(" {} ", .{nei_road == null});
        setEdge(parent, nei_road orelse self.createEdge());
    }

    fn initStandardRowEdge(self: *Graph, x: i16, y: i16) void {
        var tile: *Tile = if (self.getTile(x, y)) |t| t else return;

        tile.setLeftEdge(if (self.getTile(x - 1, y)) |t| if (t.getRightEdge()) |r| r else self.createEdge() else self.createEdge());
        tile.setRightEdge(if (self.getTile(x + 1, y)) |t| if (t.getLeftEdge()) |r| r else self.createEdge() else self.createEdge());

        // top left
        tile.setTopLeftEdge(if (self.getTile(x - 1, y - 1)) |t| if (t.getBottomRightEdge()) |r| r else self.createEdge() else self.createEdge());
        // top right
        tile.setTopRightEdge(if (self.getTile(x, y - 1)) |t| if (t.getBottomLeftEdge()) |r| r else self.createEdge() else self.createEdge());
        // bottom left
        tile.setBottomLeftEdge(if (self.getTile(x - 1, y + 1)) |t| if (t.getTopRightEdge()) |r| r else self.createEdge() else self.createEdge());
        // bottom right
        tile.setBottomRightEdge(if (self.getTile(x, y + 1)) |t| if (t.getTopLeftEdge()) |r| r else self.createEdge() else self.createEdge());
    }

    fn initOtherRowEdge(self: *Graph, x: i16, y: i16) void {
        var tile: *Tile = if (self.getTile(x, y)) |t| t else return;

        self.setOrCreateEdge(tile, x - 1, y, Tile.getRightEdge, Tile.setLeftEdge);
        self.setOrCreateEdge(tile, x + 1, y, Tile.getLeftEdge, Tile.setRightEdge);

        // top left
        self.setOrCreateEdge(tile, x, y - 1, Tile.getBottomRightEdge, Tile.setTopLeftEdge);
        // top right
        self.setOrCreateEdge(tile, x + 1, y - 1, Tile.getBottomLeftEdge, Tile.setTopRightEdge);
        // bottom left
        self.setOrCreateEdge(tile, x, y + 1, Tile.getTopRightEdge, Tile.setBottomLeftEdge);
        // bottom right
        self.setOrCreateEdge(tile, x + 1, y + 1, Tile.getTopLeftEdge, Tile.setBottomRightEdge);
    }
};

pub const St = enum(u8) {
    ROLL_COIN, // 0
    HOUSE_T,
    HOUSE_TR,
    HOUSE_BR,
    HOUSE_B,
    HOUSE_BL, // 5
    HOUSE_TL,
    ROAD_TL,
    ROAD_TR,
    ROAD_R,
    ROAD_BR, // 10
    ROAD_BL,
    ROAD_L,
    UPGRADE_T, // 13
    UPGRADE_TR,
    UPGRADE_BR, // 15
    UPGRADE_B,
    UPGRADE_BL,
    UPGRADE_TL, // 18
};

pub const Tile = struct {
    tile_type: u8,
    x: u8,
    y: u8,
    roads: [6]?*Road = [_]?*Road{null} ** 6, // weird...? I guess assigning to " = undefined" is probably undefined behavior
    houses: [6]?*House = [_]?*House{null} ** 6,

    pub fn get(self: *Tile, val: u8) switch (val) {1...6 => ?*House, 7...12 => ?*Road, 13...18 => ?*House, else => unreachable} {
        switch(val) {
            1...6 => return self.houses[val-1],
            7...12 => return self.roads[val-7],
            13...18 => return self.houses[val-13],
            else => unreachable
        }
    }

    pub fn getTopLeftEdge(self: *Tile) ?*Road       { return self.roads[0]; }
    pub fn getTopRightEdge(self: *Tile) ?*Road      { return self.roads[1]; }
    pub fn getRightEdge(self: *Tile) ?*Road         { return self.roads[2]; }
    pub fn getBottomRightEdge(self: *Tile) ?*Road   { return self.roads[3]; }
    pub fn getBottomLeftEdge(self: *Tile) ?*Road    { return self.roads[4]; }
    pub fn getLeftEdge(self: *Tile) ?*Road          { return self.roads[5]; }

    pub fn setTopLeftEdge(self: *Tile, road: ?*Road) void        { self.roads[0] = road; }
    pub fn setTopRightEdge(self: *Tile, road: ?*Road) void       { self.roads[1] = road; }
    pub fn setRightEdge(self: *Tile, road: ?*Road) void          { self.roads[2] = road; }
    pub fn setBottomRightEdge(self: *Tile, road: ?*Road) void    { self.roads[3] = road; }
    pub fn setBottomLeftEdge(self: *Tile, road: ?*Road) void     { self.roads[4] = road; }
    pub fn setLeftEdge(self: *Tile, road: ?*Road) void           { self.roads[5] = road; }

    pub fn getTopHouse(self: *Tile) ?*House         { return self.houses[0]; }
    pub fn getTopRightHouse(self: *Tile) ?*House    { return self.houses[1]; }
    pub fn getBottomRightHouse(self: *Tile) ?*House { return self.houses[2]; }
    pub fn getBottomHouse(self: *Tile) ?*House      { return self.houses[3]; }
    pub fn getBottomLeftHouse(self: *Tile) ?*House  { return self.houses[4]; }
    pub fn getTopLeftHouse(self: *Tile) ?*House     { return self.houses[5]; }

    pub fn setTopHouse(self: *Tile, house: ?*House) void         { self.houses[0] = house; }
    pub fn setTopRightHouse(self: *Tile, house: ?*House) void    { self.houses[1] = house; }
    pub fn setBottomRightHouse(self: *Tile, house: ?*House) void { self.houses[2] = house; }
    pub fn setBottomHouse(self: *Tile, house: ?*House) void      { self.houses[3] = house; }
    pub fn setBottomLeftHouse(self: *Tile, house: ?*House) void  { self.houses[4] = house; }
    pub fn setTopLeftHouse(self: *Tile, house: ?*House) void     { self.houses[5] = house; }
};

const DEFAULT_OWNER = "";
pub const Road = struct {
    id: u16,
    neighbors: [4]?*Road = [_]?*Road{null} ** 4,
    houses: [2]?*House = [_]?*House{null} ** 2,
    owner: [*:0]const u8 = DEFAULT_OWNER,

    pub fn isOwner(self: *const Road, player: str) bool {
        return self.owner == player;
    }

    pub fn setOwner(self: *Road, player: str) void {
        self.owner = player;
    }

    fn addHouse(self: *Road, house: *House) void {
        for (self.houses) |h,i| {
            if (h == house) return; // must be unique
            if (h == null) {
                self.houses[i] = house;
                return;
            }
        }
        print("\nUNREACHABLE: Road {}", .{self.id});
        for (self.houses) |r| {
            print(" HOUSE[{}]", .{r.?.id});
        }
        print("\nFailed adding ROAD[{}]\n", .{house.id});
        unreachable;
    }

    fn addNeighbor(self: *Road, nei: *Road) void {
        for (self.neighbors) |ele,i| {
            if (ele == null) {
                self.neighbors[i] = nei;
                return;
            }
        }
        unreachable;
    }
};

pub const House = struct {
    id: u16,
    roads: [3]?*Road = [_]?*Road{null} ** 3,
    owner: [*:0]const u8 = DEFAULT_OWNER,
    upgraded: bool = false,
    port_trade_id: u8 = 0,

    pub fn isOwner(self: *const House, player: str_ref) bool {
        return self.owner == player;
    }

    pub fn setOwner(self: *House, player: str_ref) void {
        self.owner = player;
    }

    fn linkToRoad(self: *House, road: *Road) void {
        self.addRoad(road);
        road.addHouse(self);
    }

    fn addRoad(self: *House, road: *Road) void {
        for (self.roads) |r, i| {
            if (r == road) {
                // print("\nnot unique", .{});
                return;
            } // must be unique
            if (r == null) {
                self.roads[i] = road;
                return;
            }
        }
        print("\nUNREACHABLE: House {}", .{self.id});
        for (self.roads) |r| {
            print(" ROAD[{}]", .{r.?.id});
        }
        print("\nFailed adding ROAD[{}]\n", .{road.id});
        unreachable;
    }
};
