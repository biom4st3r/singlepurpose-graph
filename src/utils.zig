const std = @import("std");
const Allocator = std.mem.Allocator;
const print = std.debug.print;

const str_ref: type = *const []u8;
const str: type = []const u8;
const assert = std.debug.assert;

pub fn SegArray(comptime T: type) type {
    return struct {
        const Self = @This();
        const SUB_LENGTH = 8;
        alloc: Allocator,
        i: usize = 0, // first 3 bits used for index of inner array the rest indexes the outer array
        arrays: [][]T,

        pub fn init(al: Allocator) !Self {
            return Self{
                .alloc = al,
                .arrays = undefined,
            };
        }

        pub fn deinit(self: *Self) void {
            if (self.i == 0) return;
            for (range(self.arrays.len)) |_,i| {
                self.alloc.free(self.arrays[i]);
            }
            self.alloc.free(self.arrays);
        }

        fn init_stoage(self: *Self) !void { // ![]*[]T
            self.arrays = try self.alloc.alloc([]T, 1);
            for (self.arrays) |_,i| {
                self.arrays[i] = try self.alloc.alloc(T, SUB_LENGTH);
            }
        }

        fn ensureCap(self: *Self) !void {
            if (self.i == 0) {
                try self.init_stoage();
            }
            if (self.arrays.len == self.i >> 3) {
                const old_len = self.arrays.len;
                self.arrays = try self.alloc.realloc(self.arrays, self.arrays.len + 1);
                self.arrays[old_len + 0] = try self.alloc.alloc(T, SUB_LENGTH);
            }
        }

        pub fn append(self: *Self, obj: T) !*T {
            try self.ensureCap();
            self.arrays[self.i >> 3][self.i & 0b111] = obj;
            self.i += 1;
            return self.get_last();
        }

        pub fn get_last(self: *Self) *T { 
            return &self.arrays[(self.i - 1) >> 3][(self.i - 1) & 0b111];
        }

        pub fn get(self: *Self, i: usize) *T {
            return &self.arrays[i >> 3][i & 0b111];
        }
    };
}

pub fn badintparse(chr: u8) u8 {
    if (chr < 97) {
        return chr - 48;
    } else {
        return chr - 87;
    }
}

pub fn mdarray(comptime T: type, outer: usize, inner: usize, allocator: Allocator)  ![][]T {
    const array: [][]T = try allocator.alloc([]T, outer);
    for (array) |_, i| {
        array[i] = try allocator.alloc(T, inner);
    }
    return array;
}

pub fn range(len: usize) []const void {
    return @as([*]void, undefined)[0..len];
}